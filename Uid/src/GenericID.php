<?php
namespace B9T\Uid;

/**
 * zf-utils
 *
 * @link      https://bitbucket.org/b9t/zfutils for the source repository
 * @copyright Copyright (c) 2018 Robert Böser (git@b9t.eu)
 * @license   https://www.b9t.eu/license/new-bsd New BSD License
 */

/**
 * ID generator base class.
 * This class is extended by the other ID generators contained in this module.
 */

class GenericID
{
  const CHARSET32 = '0123456789abcdefghjkmnpqrstvwxyz';
  const CHARSET62 = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';         // alnum chars, ordered by ASCII. (5,954 bit / char)
  const CHARSET64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
  const CHARSET64_WEB = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_';
  const CHARSET64_ORD = '-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';   // makes no sense in a random context ;-)

  protected $charset = self::CHARSET32;

  protected function getMilliTime()
  {
    list($u, $s) = explode(' ', microtime());
    $u = floor($u*1000);
    $u = str_pad($u, 3, '0', STR_PAD_LEFT);
    //echo "getMilliTime: $s $u\n";
    return intval($s.$u);
  }

/*
  protected function divideInt($int, $div)
  {
    $rem = $int % $div;
    $res = ($int-$rem) / $div;
    //$res1 = intval($int/$div);
    return $res;
  }
*/

  protected function getTimestampPart()
  {
    return '--- not implemented ---';
  }

  protected function getRandomPart($length)
  {
    $str = '';
    $max = strlen($this->charset) - 1;
    for ($i = 0; $i < $length; $i++) {
      $str .= $this->charset[random_int(0, $max)];
    }
    return $str;
  }

  public function __construct($mode = '')
  {
    switch($mode) {
      case 'base32':
        $this->charset = self::CHARSET32;
        break;
      case 'base62':
        $this->charset = self::CHARSET62;
        break;
      case 'base64':
        $this->charset = self::CHARSET64;
        break;
      case 'base64web':
        $this->charset = self::CHARSET64_WEB;
        break;
      case 'base64ord':
        $this->charset = self::CHARSET64_ORD;
        break;
    }
  }

  public function getID($param = null)
  {
    return '--- not implemented ---';
  }

  public function __invoke($param = null)    // 24base32, 20base64 entspricht 120bit
  {
    return $this->getID($param);
  }
}
