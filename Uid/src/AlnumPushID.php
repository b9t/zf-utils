<?php
namespace B9T\Uid;

/**
 * zf-utils
 *
 * @link      https://bitbucket.org/b9t/zfutils for the source repository
 * @copyright Copyright (c) 2018 Robert Böser (git@b9t.eu)
 * @license   https://www.b9t.eu/license/new-bsd New BSD License
 */

/**
 * ID generator that creates 20-character alpha-numeric string identifiers with the following properties:
 *
 * 1. They're based on timestamp so that they sort lexicographically by the time of creation.
 * 2. They contain about 72-bits of random data after the timestamp so that IDs won't collide with other clients' IDs.
 *
 * inspired from https://gist.github.com/mikelehen/3596a30bd69384624c11
 * 64bit integer required
 */

class AlnumPushID extends GenericID
{
  protected function getTimestampPart($unix = null)
  {
    $now = $unix ? $unix*1000 : $this->getMilliTime();
    $len = strlen($this->charset);

    $timeStampChars = [0,0,0,0,0,0,0,0];

    for ($i = 7; $i >= 0; $i--) {
      $rem = $now % $len;
      $timeStampChars[$i] = $this->charset[$rem];
      //$now = intval($now/$len);
      $now = ($now-$rem) / $len;
    }
    if($now) throw new \Exception('We should have converted the entire timestamp.');

    return implode('', $timeStampChars);
  }

  public function __construct()
  {
    // alnum chars, ordered by ASCII. (5,954 bit / char)
    parent::__construct('base62');
  }

  public function getID($param = null)    // 24base32, 20base64 entspricht 120bit
  {
    $id = $this->getTimestampPart($param) . $this->getRandomPart(12);
    if(strlen($id) != 20) throw new \Exception('Length should be 20.');
    return $id;
  }
}
