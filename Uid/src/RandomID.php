<?php
namespace B9T\Uid;

/**
 * zf-utils
 *
 * @link      https://bitbucket.org/b9t/zfutils for the source repository
 * @copyright Copyright (c) 2018 Robert Böser (git@b9t.eu)
 * @license   https://www.b9t.eu/license/new-bsd New BSD License
 */

/**
 * ID generator that creates random string identifiers with the selected character set and length.
 */

class RandomID extends GenericID
{
  public function __construct($mode = 'base32')
  {
    parent::__construct($mode);
  }

  public function getID($length = null)    // 24base32, 20base64 entspricht 120bit
  {
    if(!$length) $length = 16;
    return $this->getRandomPart($length);
  }
}
