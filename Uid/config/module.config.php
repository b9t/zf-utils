<?php
return array(
    'service_manager'   => array(
        'factories' => array(
            'B9T\Uid\AlnumPushID' => function ($sm) {
                return new \B9T\Uid\AlnumPushID();
            },
            'B9T\Uid\RandomID' => function ($sm) {
                return new \B9T\Uid\RandomID();
            },
        ),
    ),
);
