<?php
namespace B9T\Mail;

/**
 * zf-utils
 *
 * @link      https://bitbucket.org/b9t/zfutils for the source repository
 * @copyright Copyright (c) 2018 Robert Böser (git@b9t.eu)
 * @license   https://www.b9t.eu/license/new-bsd New BSD License
 */

/**
 * Send e-mail with text or HTML using SMTP.
 * A local mail server is not required.
 */

use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;

use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

class EmailSmtp
{
    protected $transport;
    protected $message;

    public function __construct()
    {
        $this->transport = new Smtp();
    }

    protected function sendParts($parts)
    {
        $body = new MimeMessage();
        $body->setParts($parts);
        $this->message->setBody($body);
        if (count($parts) > 1) {
            $this->message->getHeaders()->get('content-type')
                ->addParameter('charset', 'UTF-8')
                ->setType('multipart/alternative');
        }
        return $this->transport->send($this->message);
    }

    public function newMessage($from, $to, $subject)
    {
        $this->message = new Message();
        $this->message->addTo($to)
            ->setEncoding("UTF-8")
            ->addFrom($from)
            ->setSubject($subject);
            //->setBody($text);
    }

    public function sendPlain($text)
    {
        if (!$this->message) return;
        $text = new MimePart($text);
        $text->type = 'text/plain';
        $text->charset = 'utf-8';
        return $this->sendParts([$text]);
    }

    public function sendMixed($text, $html)
    {
        if (!$this->message) return;
        $text = new MimePart($text);
        $text->type = 'text/plain';
        $text->charset = 'utf-8';
        $html = new MimePart($html);
        $html->type = 'text/html';
        $html->charset = 'utf-8';
        return $this->sendParts([$text, $html]);
    }

    public function setOptions($opt)
    {
        $this->transport->setOptions(new SmtpOptions($opt));
    }
}
