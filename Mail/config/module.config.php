<?php
//namespace B9T\Mail;

return [
    'service_manager' => [                         // sl = serviceLocator
        'factories' => [
            'B9T\Mail\MailSmtp' => function($sl) {
                $config = $sl->get('Config');
                $mail = new B9T\Mail\EmailSmtp();
                $mail->setOptions($config['b9t']['mail']['transport']);
                return $mail;
            },
        ],
    ],
];
