<?php
namespace B9T\Mail;

class Module
{
    public function getConfig($env = null)
    {
        return include __DIR__ . '/config/module.config.php';
    }
}
