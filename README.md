# zf-utils

A collection of some general purpose modules.

* B9T\Uid : A ZF-compatible UID generator module
* b9t\Mail : A wrapper module to make sending e-mail even more simple
